/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex
%%

\n                    return 'NL'
\s+                   /* skip whitespace */
\/\/\s(.+)?\b         /* skip comments */
("2"\')([0-9]+)\b     return 'BINARY'
("10"\')([0-9]+)\b    return 'DECIMAL'
R[0-9]\b              return 'REGISTRY'

"MOV"                 return 'MOV'
"ADD"                 return 'ADD'
"SUB"                 return 'SUB'
"CMP"                 return 'CMP'

"JG"                  return 'JG'
"JE"                  return 'JE'
"JL"                  return 'JL'

"INT"                 return 'INT'

"NOP"                 return 'NOP'

<<EOF>>               return 'EOF'
.                     return 'INVALID'

/lex

/* operator associations and precedence */

%start file

%% /* language grammar */

file
    : expressions EOF
      { console.log($1); return $1; }
    ;

expressions
    :
    | expressions line
        { 
           $$ = $expressions || []; 
           if($line.constructor == Array) $$.push($line) 
         }
    ;

line
    : 'NL'
    | e 'NL'
        { $$ = $1 }
    ;

e
    : 'MOV' REGISTRY REGISTRY
        { console.log('MOV1', $2, $3); $$ = ['MOV2', $2, $3] }
    | 'MOV' REGISTRY number
        { console.log('MOV2', $2, $3); $$ = ['MOV1', $2, $3] }
    | 'ADD' REGISTRY REGISTRY
        { console.log('ADD', $2, $3); $$ =  ['ADD', $2, $3] }
    | 'SUB' REGISTRY REGISTRY
        { console.log('ADD', $2, $3); $$ =  ['SUB', $2, $3] }
    | 'CMP' REGISTRY REGISTRY
        { console.log('CMP', $2, $3); $$ =  ['CMP', $2, $3] }

    | 'JG' REGISTRY
        { console.log('JG', $2); $$ =  ['JG', $2] }
    | 'JE' REGISTRY
        { console.log('JE', $2); $$ =  ['JE', $2] }
    | 'JL' REGISTRY
        { console.log('JL', $2); $$ =  ['JL', $2] }

    | 'INT' number REGISTRY
        { console.log('INT', $2, $3); $$ =  ['INT', $2, $3] }

    | 'INT' number
        { console.log('INT', $2); $$ =  ['INT', $2, 'R1'] }
  
    | 'NOP'
        { console.log('NOP'); $$ =  ['NOP'] }
     ;

number
    : BINARY
        {  var r = /2\'([0-9]+)/.exec($1); $$ = r[1]; }
    | DECIMAL
        { var dec = /10\'([0-9]+)/.exec($1)[1]; var bin = parseInt(dec, 10).toString(2); console.log('dec:', bin); $$ = bin; }
    ;

