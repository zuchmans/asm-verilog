include <memory>;

module main
(
	fast_clock,
	clock_counter,
	clock,
	count,
	display0,
	display1,
	display2,
	display3,
	display4,
	display5,
	display6,
	display7,
	slow_clock,
	memory_address,
	memory_clock,
	memory_data,
	step,
	ip,
	instruction,
	r0,
	r1,
	r2,
	r3,
	ra,
	rb,
	data,
	tmp
);

input             fast_clock;

output reg [26:0] clock_counter;
output            clock;
output reg [3:0]  count;
output reg [6:0]  display0;
output reg [6:0]  display1;
output reg [6:0]  display2;
output reg [6:0]  display3;
output reg [6:0]  display4;
output reg [6:0]  display5;
output reg [6:0]  display6;
output reg [6:0]  display7;
output reg        slow_clock;
output reg [4:0]  memory_address;
output reg        memory_clock;
wire       [15:0] memory_data;
output     [15:0] memory_data;
output reg [3:0]  step;
output reg [4:0]  ip;
output reg [3:0]  instruction;
output reg [7:0]  r0;
output reg [7:0]  r1;
output reg [7:0]  r2;
output reg [7:0]  r3;
output reg [1:0]  ra;
output reg [1:0]  rb;
output reg [7:0]  data;
output reg [7:0]  tmp;

memory memory_module
(
	memory_address,
	memory_clock,
	memory_data
);

/*
 * Clock
 */
always@(posedge fast_clock)
begin
	clock_counter <= clock_counter + 1;
end

assign clock = clock_counter[23];

initial
begin
	memory_address <= 0;
	ip             <= 0;
	step           <= 0;
end

always@(posedge clock)
begin
	case(r0[3:0])
		4'b0000: display0 <= 7'b0000001;
		4'b0001: display0 <= 7'b1001111;
		4'b0010: display0 <= 7'b0010010;
		4'b0011: display0 <= 7'b0000110;
		4'b0100: display0 <= 7'b1001100;
		4'b0101: display0 <= 7'b0100100;
		4'b0110: display0 <= 7'b0100000;
		4'b0111: display0 <= 7'b0001111;
		4'b1000: display0 <= 7'b0000000;
		4'b1001: display0 <= 7'b0000100;
		4'b1010: display0 <= 7'b0001000;
		4'b1011: display0 <= 7'b1100000;
		4'b1100: display0 <= 7'b0110001;
		4'b1101: display0 <= 7'b1000010;
		4'b1110: display0 <= 7'b0110000;
		4'b1111: display0 <= 7'b0111000;
		default: display0 <= 7'b1111111;
	endcase
	
	case(r0[7:4])
		4'b0000: display1 <= 7'b0000001;
		4'b0001: display1 <= 7'b1001111;
		4'b0010: display1 <= 7'b0010010;
		4'b0011: display1 <= 7'b0000110;
		4'b0100: display1 <= 7'b1001100;
		4'b0101: display1 <= 7'b0100100;
		4'b0110: display1 <= 7'b0100000;
		4'b0111: display1 <= 7'b0001111;
		4'b1000: display1 <= 7'b0000000;
		4'b1001: display1 <= 7'b0000100;
		4'b1010: display1 <= 7'b0001000;
		4'b1011: display1 <= 7'b1100000;
		4'b1100: display1 <= 7'b0110001;
		4'b1101: display1 <= 7'b1000010;
		4'b1110: display1 <= 7'b0110000;
		4'b1111: display1 <= 7'b0111000;
		default: display1 <= 7'b1111111;
	endcase
	
	case(r1[3:0])
		4'b0000: display2 <= 7'b0000001;
		4'b0001: display2 <= 7'b1001111;
		4'b0010: display2 <= 7'b0010010;
		4'b0011: display2 <= 7'b0000110;
		4'b0100: display2 <= 7'b1001100;
		4'b0101: display2 <= 7'b0100100;
		4'b0110: display2 <= 7'b0100000;
		4'b0111: display2 <= 7'b0001111;
		4'b1000: display2 <= 7'b0000000;
		4'b1001: display2 <= 7'b0000100;
		4'b1010: display2 <= 7'b0001000;
		4'b1011: display2 <= 7'b1100000;
		4'b1100: display2 <= 7'b0110001;
		4'b1101: display2 <= 7'b1000010;
		4'b1110: display2 <= 7'b0110000;
		4'b1111: display2 <= 7'b0111000;
		default: display2 <= 7'b1111111;
	endcase
	
	case(r1[7:4])
		4'b0000: display3 <= 7'b0000001;
		4'b0001: display3 <= 7'b1001111;
		4'b0010: display3 <= 7'b0010010;
		4'b0011: display3 <= 7'b0000110;
		4'b0100: display3 <= 7'b1001100;
		4'b0101: display3 <= 7'b0100100;
		4'b0110: display3 <= 7'b0100000;
		4'b0111: display3 <= 7'b0001111;
		4'b1000: display3 <= 7'b0000000;
		4'b1001: display3 <= 7'b0000100;
		4'b1010: display3 <= 7'b0001000;
		4'b1011: display3 <= 7'b1100000;
		4'b1100: display3 <= 7'b0110001;
		4'b1101: display3 <= 7'b1000010;
		4'b1110: display3 <= 7'b0110000;
		4'b1111: display3 <= 7'b0111000;
		default: display3 <= 7'b1111111;
	endcase
	
	case(r2[3:0])
		4'b0000: display4 <= 7'b0000001;
		4'b0001: display4 <= 7'b1001111;
		4'b0010: display4 <= 7'b0010010;
		4'b0011: display4 <= 7'b0000110;
		4'b0100: display4 <= 7'b1001100;
		4'b0101: display4 <= 7'b0100100;
		4'b0110: display4 <= 7'b0100000;
		4'b0111: display4 <= 7'b0001111;
		4'b1000: display4 <= 7'b0000000;
		4'b1001: display4 <= 7'b0000100;
		4'b1010: display4 <= 7'b0001000;
		4'b1011: display4 <= 7'b1100000;
		4'b1100: display4 <= 7'b0110001;
		4'b1101: display4 <= 7'b1000010;
		4'b1110: display4 <= 7'b0110000;
		4'b1111: display4 <= 7'b0111000;
		default: display4 <= 7'b1111111;
	endcase
	
	case(r2[7:4])
		4'b0000: display5 <= 7'b0000001;
		4'b0001: display5 <= 7'b1001111;
		4'b0010: display5 <= 7'b0010010;
		4'b0011: display5 <= 7'b0000110;
		4'b0100: display5 <= 7'b1001100;
		4'b0101: display5 <= 7'b0100100;
		4'b0110: display5 <= 7'b0100000;
		4'b0111: display5 <= 7'b0001111;
		4'b1000: display5 <= 7'b0000000;
		4'b1001: display5 <= 7'b0000100;
		4'b1010: display5 <= 7'b0001000;
		4'b1011: display5 <= 7'b1100000;
		4'b1100: display5 <= 7'b0110001;
		4'b1101: display5 <= 7'b1000010;
		4'b1110: display5 <= 7'b0110000;
		4'b1111: display5 <= 7'b0111000;
		default: display5 <= 7'b1111111;
	endcase
	
	case(r3[3:0])
		4'b0000: display6 <= 7'b0000001;
		4'b0001: display6 <= 7'b1001111;
		4'b0010: display6 <= 7'b0010010;
		4'b0011: display6 <= 7'b0000110;
		4'b0100: display6 <= 7'b1001100;
		4'b0101: display6 <= 7'b0100100;
		4'b0110: display6 <= 7'b0100000;
		4'b0111: display6 <= 7'b0001111;
		4'b1000: display6 <= 7'b0000000;
		4'b1001: display6 <= 7'b0000100;
		4'b1010: display6 <= 7'b0001000;
		4'b1011: display6 <= 7'b1100000;
		4'b1100: display6 <= 7'b0110001;
		4'b1101: display6 <= 7'b1000010;
		4'b1110: display6 <= 7'b0110000;
		4'b1111: display6 <= 7'b0111000;
		default: display6 <= 7'b1111111;
	endcase
	
	case(r3[7:4])
		4'b0000: display7 <= 7'b0000001;
		4'b0001: display7 <= 7'b1001111;
		4'b0010: display7 <= 7'b0010010;
		4'b0011: display7 <= 7'b0000110;
		4'b0100: display7 <= 7'b1001100;
		4'b0101: display7 <= 7'b0100100;
		4'b0110: display7 <= 7'b0100000;
		4'b0111: display7 <= 7'b0001111;
		4'b1000: display7 <= 7'b0000000;
		4'b1001: display7 <= 7'b0000100;
		4'b1010: display7 <= 7'b0001000;
		4'b1011: display7 <= 7'b1100000;
		4'b1100: display7 <= 7'b0110001;
		4'b1101: display7 <= 7'b1000010;
		4'b1110: display7 <= 7'b0110000;
		4'b1111: display7 <= 7'b0111000;
		default: display7 <= 7'b1111111;
	endcase
end

always@(posedge clock)
begin
	case(step)
		0:
		// Prepare memory to read
		begin
			memory_clock   <= 0;
			memory_address <= ip;
			
			step           <= 1;
		end
		
		1:
		// Fetch data from memory on posedge
		begin
			memory_clock   <= 1;
			
			step           <= 2;
		end
		
		2:
		// Read instruction data
		begin
			instruction    <= memory_data[15:12];
			ra             <= memory_data[11:10];
			rb             <= memory_data[9:8];
			data           <= memory_data[7:0];
			
			step           <= 3;
		end
		
		3:
		// First step of instruction execution
		begin
			case(instruction)
			0:
			// MOV <register> <- <constant>
			begin
				case(ra)
					0: r0 <= data;
					1: r1 <= data;
					2: r2 <= data;
					3: r3 <= data;
				endcase
			end
			
			1:
			// MOV <register1> <- <register2>
			begin
				case(rb)
					0: data <= r0;
					1: data <= r1;
					2: data <= r2;
					3: data <= r3;
				endcase
			end
			
			2:
			// MOV <memory> -> <register>
			begin
				// TODO
			end
			
			3:
			// MOV <register> -> <memory>
			begin
				// TODO
			end
			
			4:
			// ADD <register1> <register2>
			begin
				case(rb)
					0: data <= r0;
					1: data <= r1;
					2: data <= r2;
					3: data <= r3;
				endcase;
			end
			endcase
			
			step <= 4;
		end
		
		4:
		// Second step of instruction execution
		begin
			case(instruction)
			1:
			// MOV <register1> <- <register2>
			begin
				case(ra)
					0: r0 <= data;
					1: r1 <= data;
					2: r2 <= data;
					3: r3 <= data;
				endcase
			end
			
			4:
			begin
				case(ra)
					0: r0 <= r0 + data;
					1: r1 <= r1 + data;
					2: r2 <= r2 + data;
					3: r3 <= r3 + data;
				endcase
			end
			endcase
			
			step <= 5;
		end
		
		5:
		// Increment instruction pointer
		begin
			ip <= ip + 1;
			
			step <= 0;
		end
	endcase
end
endmodule